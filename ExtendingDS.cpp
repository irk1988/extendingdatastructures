/*
 * Author: Philip Karunakaran, Immanuel Rajkumar
 * Extending Data Structures
 * C++ STL Containers - Set, MultiMap have been used as Red Black Trees with appropriate modifications
 */

#include<iostream>
#include<sstream>
#include<set>
#include<map>
#include<utility>
#include<limits>
#include<list>
#include<iomanip>

using namespace std;


/*
 * Structure of an item to be stored
 * Name parts of an item are stored in a different datastructure - an RB Tree which allows multiple entries.
 */
struct Item {
    long int id;
    double price;

    Item( long int id, double price) {
        this->id = id;
        this->price = price;
    }
};

/*
 * Wrapper ID class for storing pointers of Items
 * This structure helps order pointers based on ID and stor it in containers in STL
 */
struct Wrapper_ID {
    struct Item * ptr;
};

bool isInsert; //Global variable to track if it is an insert opration
/*
 * Overloading the < operator to sort items based on ID
 * While insert, the new price is replaced by old price if the item already exisits
 */
inline bool operator <(const Wrapper_ID& l_item, const Wrapper_ID& r_item )
{
    if (l_item.ptr -> id  == r_item.ptr -> id  && isInsert )
        r_item.ptr->price = l_item.ptr->price;
    return (l_item.ptr -> id)  < (r_item.ptr -> id);
}


/*
 * Class implementing various operations on Items as specified in problem statement
 */
class Store
{
private:
    /*
     * Data Structures used for storing indexes of the items
     */
    set<Wrapper_ID> sp; //Red Black Tree to store Items with, item ID, item price in a structure Item.
    multimap<long int, Wrapper_ID> sp3; //Red Black Tree to store Item Name Parts and its corresponding item pointer wrapped in a class Wrappr_ID

public:
    /*
     * Input - Item ID, Item Price, Name of the Item
     * Insert if it is a new item or replace the price and name if it is an old item
     * Return 1 if new item is inserted, else 0 if it is a replacement operation
     */
    int insert ( long int id, double price, list<long int> namePart ) {
        int retvalue = 0;
        Item *i = new Item(id, price);
        Wrapper_ID iwr ;
        iwr.ptr = i;

        isInsert = true;
        pair<set<Wrapper_ID>::iterator,bool> p;
        p = sp.insert(iwr); // Insert item into RBT, price will be replaced if item already exists
        retvalue = p.second;

        if( !namePart.empty() ) {
            if( retvalue == 0 ) {
                //Delete the old name parts for the item if it is a replacement operation
                for( multimap<long int, Wrapper_ID>::iterator i = sp3.begin(); i !=sp3.end(); i++ )
                    if( ((*i).second).ptr->id == id )
                        sp3.erase(i);

                Item * k = (*sp.find(iwr)).ptr;
                iwr.ptr = k;
            }
        }

        //Insert new name parts for the item
        for ( list<long int>::iterator i = namePart.begin(); i!=namePart.end(); i++ )
            sp3.insert(pair<long int,Wrapper_ID>(*i,iwr));

        isInsert = false;
        return retvalue;
    }


    /*
     * Input - Item ID
     * Find the item with given input
     * Return the price of the found item, 0 if the item is not found
     */
    double find( long int id ) {
        double retvalue = 0;
        Wrapper_ID iTemp;
        Item * y = new Item(id, 0);
        iTemp.ptr = y;

        //Find the item with the given id
        set<Wrapper_ID>::iterator k = sp.find(iTemp);
        if( k != sp.end() )
            retvalue = (*k).ptr->price;

        return retvalue;
    }


    /*
     * Input - Item ID
     * Delete the item with given input
     * Return sum of the name parts of the deleted item, 0 otherwise
     */
    long long int deleteItem( long int id ) {
        long long int retvalue = 0;
        Item * y = new Item(id, 0);
        Wrapper_ID iTemp;
        iTemp.ptr = y;

        set<Wrapper_ID>::iterator k = sp.find(iTemp);
        if( k != sp.end() ) {
            long int tid = (*k).ptr->id;
            long long int namesum = 0;
            //Delete the name parts of the items from the name part RB Tree
            for( multimap<long int, Wrapper_ID>::iterator i = sp3.begin(); i !=sp3.end(); i++ )
                if( ((*i).second).ptr->id == tid ) {
                    namesum += (*i).first;
                    sp3.erase(i);
                }

            //Delete the item from item RB Tree
            sp.erase(k);
            retvalue = namesum;
        }

        return retvalue;
    }

    /*
     * Input - Name part of an Item
     * Return the lowest price among those items with the given name part in their name
     */
    double findMinPrice ( long int namePart ) {
        double MAX = std::numeric_limits<double>::max();
        double minPrice = MAX;

        //Find items from the name part RB tree with the given name part
        pair<multimap<long int, Wrapper_ID>::iterator,multimap<long int, Wrapper_ID>::iterator>  ppp;
        ppp = sp3.equal_range(namePart);

        //From the selected items, find the minimum price
        for( multimap<long int, Wrapper_ID>::iterator i = ppp.first; i != ppp.second; ++i )
            if( ((*i).second).ptr->price < minPrice )
                minPrice = ((*i).second).ptr->price;

        if( minPrice == MAX ) minPrice = 0;
        return minPrice;
    }

    /*
     * Input - Name part of an Item
     * Return the highest price among those items with the given name part in their name
     */
    double findMaxPrice ( long int namePart ) {
        double maxPrice = 0;
        //Find items from the name part RB tree with the given name part
        pair<multimap<long int, Wrapper_ID>::iterator,multimap<long int, Wrapper_ID>::iterator>  ppp;
        ppp = sp3.equal_range(namePart);

        //From the selected items, find the maximum price
        for( multimap<long int, Wrapper_ID>::iterator i = ppp.first; i != ppp.second; ++i )
            if( ((*i).second).ptr->price >= maxPrice )
                maxPrice = ((*i).second).ptr->price;

        return maxPrice;
    }

    /*
     * Input - Name part of an Item, Price range [minPrice, maxPrice]
     * Return the count of items with the given name part and those fall withing the given price range
     */
    long int findPriceRange( long int namePart, double minPrice, double maxPrice ) {
        long int count = 0;
        //Find items from the name part RB Tree with the given name
        pair<multimap<long int, Wrapper_ID>::iterator,multimap<long int, Wrapper_ID>::iterator>  ppp;
        ppp = sp3.equal_range(namePart);

        //From the selected items, count the number of items that fall within the range
        for( multimap<long int, Wrapper_ID>::iterator i = ppp.first; i != ppp.second; i++ )
            if( ((*i).second).ptr->price >= minPrice && ((*i).second).ptr->price <= maxPrice )
                count++;
        return count;
    }

    /*
     * Input - Item ID range [minID, maxID] , price increment rate
     * Increment the price of all items with their ID in the given range by give rate
     * Return the net price increase
     */
    double priceHike ( long int minID, long int maxID, double incRate ) {
        double pricerise = 0 , retvalue = 0;
        Wrapper_ID piMin;
        Wrapper_ID piMax;
        Item * i1 = new Item(minID, 0);
        Item * i2 = new Item(maxID, 0);
        piMin.ptr = i1;
        piMax.ptr = i2;

        //Find the list of items in the item ID range
        set<Wrapper_ID>::iterator ii, iup, ilow;
        ilow = sp.lower_bound(piMin);
        iup = sp.upper_bound(piMax);

        //Modify the price of each of the items in the selected range
        for( ii = ilow; ii != iup; ++ii ) {
            pricerise = 0;
            pricerise = (incRate/100) * ((*ii).ptr->price);

            int isign = pricerise > 0 ? 1 : -1;
            unsigned long int tempNum = (pricerise * 100) * isign;
            pricerise = ((double)tempNum)/100 * isign;

            (*ii).ptr->price += pricerise;
            retvalue +=  pricerise;
        }
        return retvalue;
    }
};

/* Main Program */
int main()
{
    /*
     * Variables used for parsing the driver program
     */
    string cmd , s;
    long int id = 0, name = 0, namePart = 0;
    double price = 0 , minPrice = 0, maxPrice = 0, incRate = 0;
    long double sum = 0, retvalue=0; // Return value of the program
    long int counter = 1;

    /*Utility variables */
    double pricesearch = 0, pricerise=0;
    Wrapper_ID iTemp;

    Store store;

    while( getline(cin,s) ) {
        if( s[0] == '#' ) continue; //Skip command if comment
        stringstream ss(s); //Using string stream to parse each command in the driver program
        ss >> cmd;
        retvalue = 0;

        /*
         * Check the operation to be performed based on the input command in the driver program
         */
        if( cmd == "Insert" ) {
            //cout<<counter<<": Insert: ";
            ss >> id >> price;
            ss >> name;
            list<long int> namePart;
            while ( name != 0 ) {
                namePart.push_back(name);
                ss >> name;
            }
            retvalue = store.insert(id, price, namePart);
        } else if( cmd == "Find" ) {
            ss >> id;
            //cout<<counter<<": Find: ";
            retvalue = store.find(id);

        } else if( cmd == "Delete" ) {
            ss >> id;
            //cout<<counter<<": Delete: ";
            retvalue = store.deleteItem(id);
        } else if( cmd == "FindMinPrice" ) {
            ss >> namePart;
            //cout<<counter<<": FindMinPrice: ";
            retvalue = store.findMinPrice(namePart);
        } else if( cmd == "FindMaxPrice" ) {
            ss >> namePart;
            //cout<<counter<<": FindMaxPrice: ";
            retvalue = store.findMaxPrice(namePart);
        } else if( cmd == "FindPriceRange" ) {
            ss >> namePart >> minPrice >> maxPrice;
            //cout<<counter<<": FindPriceRange: ";
            retvalue = store.findPriceRange( namePart, minPrice, maxPrice );
        } else if( cmd == "PriceHike" ) {
            long int minID, maxID;
            ss >> minID >> maxID >> incRate;
            //cout<<counter<<": PriceHike: ";
            retvalue = store.priceHike(minID, maxID, incRate);
        } else cout<<"\nUnknown Operation";
        //cout<<retvalue<<endl;
        sum += retvalue;
        counter++;
    }
    cout<<std::fixed<<setprecision(2)<<showpoint<<sum<<endl;
    return 0;
}
